#!/usr/bin/env python3

import json
import sys
import os
import paho.mqtt.client as mqtt

# local module OSC3.py
from OSC3 import *

class OSCSender:
    def __init__(self, serverport=None):
        self.osc = OSCClient()
        if serverport:
            self.osc.connect(serverport)
    def connect(self, serverport):
        if self.osc.address():
            print("ignoring old address")
        self.osc.connect(serverport)
    def send(self, msg, data=None):
        if type(msg) in [OSCMessage, OSCBundle]:
            return self.osc.send(msg)
        return self.osc.send(OSCMessage(msg, data))

class Connector:
    def __init__(self, osc, sensors):
        self.osc = osc
        self.sensors = sensors

    def on_connect(self, client, userdata, flags, rc):
        print('connected with result code={}'.format(rc))
        topiclist = []
        for sensor in self.sensors:
            topiclist.append(('+/sensors/{}/+'.format(sensor), 0))
            print('added sensor {} to list'.format(sensor))
        client.subscribe(topiclist)

    def mqtt2osc(self, topic, pdict):
        osctree = '/{}'.format(topic)
        for tag in pdict['tags']:
            osctree += '/{}/{}'.format(tag, pdict['tags'][tag])
        for value in pdict['values']:
            self.osc.send('{}/{}'.format(osctree, value), pdict['values'][value])

    def on_message(self, client, obj, msg):
        self.mqtt2osc(msg.topic, json.loads(msg.payload.decode()))

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('no config file, exiting')
        sys.exit(1)

    if not os.path.isfile(sys.argv[1]):
        print('configfile "{}" not found, exiting'.format(sys.argv[1]))
        sys.exit(1)

    with open(sys.argv[1], 'r') as configfile:
        config = json.load(configfile)

    connector = Connector(OSCSender((config['osc_ip'], config['osc_port'])), config['mqtt_src']['sensors'])

    try:
        client = mqtt.Client()
        client.on_connect = connector.on_connect
        client.on_message = connector.on_message
        client.connect(config['mqtt_src']['ip'], config['mqtt_src']['port'], 60)
        print('connecting to {}:{}'.format(
            config['mqtt_src']['ip'], config['mqtt_src']['port']
        ))
        client.loop_forever()
    except KeyboardInterrupt:
        print('exiting')
    except Exception as e:
        print('terminating on error: ', e)
