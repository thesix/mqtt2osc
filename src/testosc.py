#!/usr/bin/env python3

from OSC3 import *

s = OSCServer(('127.0.0.1', 9999))
s.addDefaultHandlers()
s.serve_forever()
