# Description of sensors and data sources

## cyborg2

`cyborg2` currently serves data from two sensors.  One `VEML6075` UVA and UVB light sensor and one `BME280` measuring relative humidity, barometric pressure and ambient temperature.

### VEML6075

OSC paths to data from `VEML6075`:
```
/cyborg2/sensors/VEML6075/luminosity/id/i2c-1_0x10/channels/4/exposure_s/0.05/interval_s/0.1/UVA_raw
/cyborg2/sensors/VEML6075/luminosity/id/i2c-1_0x10/channels/4/exposure_s/0.05/interval_s/0.1/UVB_raw
/cyborg2/sensors/VEML6075/luminosity/id/i2c-1_0x10/channels/4/exposure_s/0.05/interval_s/0.1/visible_raw
/cyborg2/sensors/VEML6075/luminosity/id/i2c-1_0x10/channels/4/exposure_s/0.05/interval_s/0.1/ir_raw
```
### BME280

OSC paths to data from `BME280`:
```
/cyborg2/sensors/BME280/temperature/id/0x76/temperature_degC
/cyborg2/sensors/BME280/pressure/id/0x76/pressure_hPa
/cyborg2/sensors/BME280/humidity/id/0x76/humidity_rel_percent
```
[BME280 description](https://www.bosch-sensortec.com/products/environmental-sensors/humidity-sensors-bme280/)

## cyborg3

`cyborg3` currently hosts three sensors.  One `MPU9250` with a 3-axis gyroscope, a 3-axis accelerometer and a 3-axis magnetometer, one `TYPE5` pocket geiger radiation sensor and one `SPS30` sensor measuring dust particles.

### MPU9250

OSC paths to data from `MPU9250`:
```
/cyborg3/sensors/MPU9250/acceleration/axis/x/acceleration_g
/cyborg3/sensors/MPU9250/acceleration/axis/y/acceleration_g
/cyborg3/sensors/MPU9250/acceleration/axis/z/acceleration_g
/cyborg3/sensors/MPU9250/rotation/axis/x/rotation_dps
/cyborg3/sensors/MPU9250/rotation/axis/y/rotation_dps
/cyborg3/sensors/MPU9250/rotation/axis/z/rotation_dps
/cyborg3/sensors/MPU9250/magnetic/axis/x/magnetic_field_uT
/cyborg3/sensors/MPU9250/magnetic/axis/y/magnetic_field_uT
/cyborg3/sensors/MPU9250/magnetic/axis/z/magnetic_field_uT
/cyborg3/sensors/MPU9250/temperature/temperature_degC
```
[MPU9250 description](https://invensense.tdk.com/wp-content/uploads/2015/02/PS-MPU-9250A-01-v1.1.pdf)

### TYPE5

OSC paths to data from `TYPE5`:
```
/cyborg3/sensors/TYPE5/radiation/radiation_cps
/cyborg3/sensors/TYPE5/radiation/radiation_Svph
/cyborg3/sensors/TYPE5/type5/type5_errorSvph
/cyborg3/sensors/TYPE5/type5/type5_duration
/cyborg3/sensors/TYPE5/type5/type5_raycount
```
[Type5 description](https://www.sparkfun.com/products/14209)

### SPS30

OSC paths to data from `SPS30`:
```
/cyborg3/sensors/SPS30/particulate_matter/interval_s/1/id/UART0-2A7B56F3D1787EC4/cleaning_interval_d/4/p10_ugpm3
/cyborg3/sensors/SPS30/particulate_matter/interval_s/1/id/UART0-2A7B56F3D1787EC4/cleaning_interval_d/4/p4_ugpm3
/cyborg3/sensors/SPS30/particulate_matter/interval_s/1/id/UART0-2A7B56F3D1787EC4/cleaning_interval_d/4/p2.5_ugpm3
/cyborg3/sensors/SPS30/particulate_matter/interval_s/1/id/UART0-2A7B56F3D1787EC4/cleaning_interval_d/4/p1_ugpm3
/cyborg3/sensors/SPS30/particulate_matter/interval_s/1/id/UART0-2A7B56F3D1787EC4/cleaning_interval_d/4/p10_ppcm3
/cyborg3/sensors/SPS30/particulate_matter/interval_s/1/id/UART0-2A7B56F3D1787EC4/cleaning_interval_d/4/p4_ppcm3
/cyborg3/sensors/SPS30/particulate_matter/interval_s/1/id/UART0-2A7B56F3D1787EC4/cleaning_interval_d/4/p2.5_ppcm3
/cyborg3/sensors/SPS30/particulate_matter/interval_s/1/id/UART0-2A7B56F3D1787EC4/cleaning_interval_d/4/p1_ppcm3
/cyborg3/sensors/SPS30/particulate_matter/interval_s/1/id/UART0-2A7B56F3D1787EC4/cleaning_interval_d/4/p0.5_ppcm3
/cyborg3/sensors/SPS30/particulate_matter/interval_s/1/id/UART0-2A7B56F3D1787EC4/cleaning_interval_d/4/typpartsize_um
```
[SPS30 description](https://www.sensirion.com/en/environmental-sensors/particulate-matter-sensors-pm25/)
