# mqtt2osc

Retreive messages from an MQTT broker and translate them to OSC so they can
be used in sound/multimedia applications.  This project is designed for *esc
medien kunst labors* project *urban cyborgs*.

## Prerequisites

We need the `paho-mqtt` python3 module.  On `Debian` based systems run:
```
sudo apt install python3-paho-mqtt
```

## Usage

Rename `config.json.example` to `config.json`, edit to your needs and run
```
./src/mqtt2osc.py config.json
```

## Documentation

Checkout [SensorDescription](SensorDescription.md) to see what kind of data sensors produce and how to access it.
